db.persons.insertMany(
//data
)

db.getCollection('persons').find({}).count()

//Find Records with name and age
db.getCollection('persons').find({"age":20})
db.getCollection('persons').find({"name":"Aurelia Gonzales"})
db.getCollection('persons').find({"gender":"female","eyeColor":"green"}).count()


//Comparison Operators

$eq == equal, 
$gt == greater than, 
$lt == less than, 
$ne == not equal,
$gte == greater than equal, 
$lte == less than equal 
$in  == in
$nin == not in

//Find entries where colour not equal to green
db.getCollection('persons').find({"eyeColor":{"$ne":"green"}})

db.getCollection('persons')
.find({"age":{"$gte":25, "$lte":30 }})

db.getCollection('persons')
.find({"name":{"$gt":"N"}})
.sort({"name":1})

////// in and not in

db.getCollection('persons')
.find({"age":{"$in":[21,22]}})

//and
db.getCollection('persons')
.find({$and: [{gender:"female"},{favoriteFruit:"banana"}]})


//or
db.getCollection('persons')
.find({$or:[{isActive: true},{eyeColor:"blue"}]})

db.getCollection('persons')
.find({$or:[{eyecolor: "green"},{eyeColor:"blue"}]})

.find({eyecolor:{$in:["green","blue"]}})


//nested 
db.getCollection('persons')
.find({"company" : {
        "title" : "ISONUS",
        "email" : "daleholman@isonus.com",
        "phone" : "+1 (871) 452-3036",
        "location" : {
            "country" : "Italy",
            "address" : "586 Blake Court"
        }}})


db.getCollection('persons')
.find({"company.location.country":"USA","company.title":"OVOLO"})


////////////////////////////////////////////////////

//Query Array

db.getCollection('persons')
.find({"tags" : [ 
        "enim", 
        "id", 
        "velit", 
        "ad", 
        "consequat"
    ]})

db.getCollection('persons')
.find({tags:"ad"})

//gets first element
db.getCollection('persons')
.find({tags.0:"ad"})

//all operator
db.getCollection('persons')
.find({tags:{$all: ["id","ad"]}})


db.getCollection('first')
.find({"friends.name":"Lara"})

db.getCollection('first')
.find({friends:{
       "name":"Lara",
        "age":27
}})

db.getCollection('first')
.find({"friends.name":"Steve","friends.age":27})

///////////////////////////////////////////////////

$elemMatch

db.getCollection('first')
.find({friends:{$elemMatch:{"age":25,"name":"Lora","registered":true}}})

Element Operators
$exists , &type

db.getCollection('persons').find({surname:{$exists:false}}).count()

db.getCollection('persons').find({index:{$type:"int",$eq:10}})

////////////////////////////////////////////////////

Filter Fields

db.getCollection('persons')
.find({},{name:1,age:1,eyecolor:1})

//Output

    "_id" : ObjectId("63f5d08c4e3e3b54d4b35b78"),
    "name" : "Aurelia Gonzales",
    "age" : 20
}

/* 2 */
{
    "_id" : ObjectId("63f5d08c4e3e3b54d4b35b79"),
    "name" : "Kitty Snow",
    "age" : 38
}


//All records will be visible except name and age
db.getCollection('persons')
.find({},{name:0,age:0})

/////////////////////////////////////////

Regex

db.getCollection('persons')
.find({name:{regex:/rel/,$options:"i"}})

db.getCollection('persons')
.find({name:{$regex:"^Aur"}})

/////////////////////////////////////////////////

db.shoppingCart.update(
{},
{$set: {processed:false}},
{multi:true}
)

//Update One

db.getColllection('shoppingCart')
.updateOne(
//query
{cartId:325},
//update
{$set:{
   processed:true
}},

//update options
{}
)


// Update Many

db.getCollection('shoppingCart')
.updateMany(
{cart:{$exists:false}},
{$set : {
   cart: []
}},

{}
)


/////////////////////////////////////////

Replace

db.getCollection('shoppingCart')
.replaceOne(
{"index":1},
{
    "index":1,
    "processed":true,
    "cart":["item1","item2"]
},

{}
)

// Update multiple

db.getCollection('shoppingCart')
.updateOne(
{"index":4},
{
    $set:{
      cartId: NumberInt(435),
      "customer.name":"Samanta Larsen",  
    },
    $unset: {
        newOrder : 1
    }
},

{}
)

// Rename

db.getCollection('shoppingCart')
.updateMany(
{cartId: {$exists : true}},
  {
    $rename: {
        cartId : "orderId"
    }
},

{}
)


// Update Date

db.getCollection('shoppingCart')
.updateOne(
{index:1},
  {
    $set: {
        updatedAt : new Date()
    }
},

{}
)

/////////////////////////////////////////////////////

Array Update Operators

$ , $push , $addToSet , $pull , $pullAll , $pop


// push

db.getCollection('shoppingCart')
.updateOne(
{cartId : 561},
{
    $push:{
        cart: "item1"
        }
    },
 {}   
)

// addToSet

db.getCollection('shoppingCart')
.updateOne(
{cartId : 561},
{
    $addToSet:{
        cart: "item2"
        }
    },
 {}   
)

// pop

db.getCollection('shoppingCart')
.updateOne(
{cartId : 561},
{
    $pop:{
        cart: "1"
        }
    },
 {}   
)

// pull

db.getCollection('shoppingCart')
.updateOne(
{cartId : 561},
{
    $pull:{
        spentAmounts: {$gt:500}
        }
    },
 {}   
)


// pull All

db.getCollection('shoppingCart')
.updateOne(
{cartId : 561},
{
    $pullAll:{
        spentAmounts: [400,500]
        }
    },
 {}   
)

//Positional Operator

db.getCollection('shoppingCart')
.updateOne(
{cartId : 561, cart:"item2"},
{
    $set:{
         "cart.$":"updatedItem2"
        }
    },
 {}   
)


////  Nested Positional

db.getCollection('shoppingCart')
.updateOne(
{cartId : 561},
{
    $push:{
         cart:{$each:[{"title":"Tv"},{"title":"Phone"}]}
        }
    },
 {}   
)


db.getCollection('shoppingCart')
.updateOne(
{cartId : 561},
{
    $set:{
         "cart.$.price":NumberInt(340),
         "cart.$.quantity":NumberInt(2),
        }
    },
 {}   
)

// Postional Operator $ with $elemMatch

db.getCollection('shoppingCart')
.updateOne(
{
    "cartId" : 456,
    cart :{$elemMatch:{
        title:"Phone",
        price:150
    }}
    },
    {
        $set:{
            "cart.$.quantity":NumberInt(2)
            }
        },
 {}   
)

////////////////////////////////////////

$inc

db.getCollection('shoppingCart')
.updateOne(
{cartId:325},
{
    $inc:{
        totalCartItems:NumberInt(1)}}
   
)

